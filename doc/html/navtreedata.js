/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "cutex", "index.html", [
    [ "Klassen", "annotated.html", [
      [ "Auflistung der Klassen", "annotated.html", "annotated_dup" ],
      [ "Klassen-Verzeichnis", "classes.html", null ],
      [ "Klassenhierarchie", "hierarchy.html", "hierarchy" ],
      [ "Klassen-Elemente", "functions.html", [
        [ "Alle", "functions.html", "functions_dup" ],
        [ "Funktionen", "functions_func.html", "functions_func" ],
        [ "Aufzählungen", "functions_enum.html", null ],
        [ "Aufzählungswerte", "functions_eval.html", null ],
        [ "Verwandte Funktionen", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Dateien", "files.html", [
      [ "Auflistung der Dateien", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classcutex_1_1_qx_help_window.html",
"classcutex_1_1_qx_sql_database.html#ae3ee9783dd8d7ab77454abd5fb5fbc8d",
"classcutex_1_1_qx_text_edit.html#abfdae775e1de35416e0488349e8d0e5da3de9a7b93626f35d888d4822bccf4872",
"functions_w.html"
];

var SYNCONMSG = 'Klicken um Panelsynchronisation auszuschalten';
var SYNCOFFMSG = 'Klicken um Panelsynchronisation einzuschalten';